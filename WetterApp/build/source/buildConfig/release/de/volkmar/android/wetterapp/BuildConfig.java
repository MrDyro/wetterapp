/**
 * Automatically generated file. DO NOT MODIFY
 */
package de.volkmar.android.wetterapp;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String PACKAGE_NAME = "de.volkmar.android.wetterapp";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
}
