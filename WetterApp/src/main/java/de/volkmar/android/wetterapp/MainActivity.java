package de.volkmar.android.wetterapp;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener {

	private String[] stringArrayNames;
	private ArrayAdapter<String> stringArrayAdapter;
	private EditText etInput;
	private ArrayList<String> arrayListNames;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		stringArrayNames = new String[]{"Berlin", "Hamburg"};

		arrayListNames = new ArrayList<String>();
		arrayListNames.addAll(Arrays.asList(stringArrayNames));

		ListView lvListView = (ListView) findViewById(R.id.lvList);
		etInput = (EditText) findViewById(R.id.etInput);
		Button btnAdd = (Button) findViewById(R.id.btnAdd);

		stringArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayListNames);

		lvListView.setAdapter(stringArrayAdapter);
		lvListView.setOnItemClickListener(this);

		btnAdd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String strInput = etInput.getText().toString();
				if (!TextUtils.isEmpty(strInput)) {
					stringArrayAdapter.add(strInput);
					etInput.setText("");
				}
			}
		});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {

		Toast.makeText(this, arrayListNames.get(index), Toast.LENGTH_SHORT).show();

		Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
		intent.putExtra(SearchManager.QUERY, "weather in " + arrayListNames.get(index));
		startActivity(intent);
	}
}
